# 排線 M 邊的不對稱檢測

此專案為 AIA（人工智慧學校）期末專題，題目為排線 M 邊的不對稱檢測，所謂「對稱」意指銅線部分要在整個排線的中間；反之則偏上或偏下。如下二圖分別為兩個排線，藍色與紅色標示出每個排線的兩個 M 邊，右圖為負樣本（不對稱），銅線區域偏上，導致藍色區塊面積略小於紅色區塊；反之，左圖為正樣本（對稱），藍紅面積相近。

![M Side Demo](assets/m_side_demo_0.jpg) ![M Side Demo](assets/m_side_demo_1.jpg)

The repository includes：
* MASK RCNN 的資料前處理
* 以 MASK RCNN 來做銅線區塊 object detection 的 source code
* Unet 的資料前處理
* 以 Unet 來做 M 邊 image segmentation 的 source code
* 整合 Prediction code in summary

# Training Model

訓練流程：
1. 用 Labelme 標記出排線的前段
2. 以 MASK RCNN detect 出排線的前段
3. 以 MASK RCNN detection result 進行圖片旋轉、裁切，得到兩個 M 邊各自的小圖
4. 用 Labelme 標記 step3 小圖 M 邊的區塊後，進行 Unet segmentation training
5. 計算兩個 M 邊的面積差

### 1. MASK RCNN 資料前處理

[labelme_to_dataset_mask_rcnn.ipynb](labelme_to_dataset_mask_rcnn/labelme_to_dataset_mask_rcnn.ipynb)

先用 Labelme 標記出排線的前段，產生出 json 檔（紀錄標記的 points 和 base64 編碼後的原圖）。由於拍照時都排列都很工整、方向一致，所以對產出後的 json 檔每 60 度旋轉一次，以 圖片中心為選轉中心，將 points 乘上選轉矩陣，再將原圖旋轉後轉 base64 編碼，存入 json 的 imageData，輸出旋轉後的圖片以及 json 檔。

**Note：** 選轉有其必要。我們發現當 traing dataset 沒有旋轉，排列都一致時，在 prediction 階段讓 model predict 一張 rotated 的排線圖片，效果很差。

**Note：** 確認 labelme 為 3.3.1 版本

![Labelme Demo](assets/step1_labelme.png) ![Labelme Rotated Demo](assets/step1_labelme_rotated.png)

### 2. Training MASK RCNN, Getting Bonding Box & Masked Region

[MaskRcnn.ipynb](mask_rcnn/MaskRcnn.ipynb)

訓練出 MASK RCNN model 後，預測出排線銅線區塊的 bonding box 和 masked 區域，得到的 bonding box 4 個值（left, top, right, bottom）以及 masked 區域邊界的每一個點存入 [json 檔（連結是範例）](assets/step2_mask_rcnn_label.json)，若將兩者作圖輸出，會如右圖，紅色是 bonding box；白色匡出的是 segmentation 結果的邊界。

**Note：** 由於 MASK RCNN 需要 tendorflow==1.10 的版本限制，這方面的運行是在 AWS Sagemaker 上執行的，使用 conda_python3 的環境，並手動安裝 tensorflow 以及 dependences。

![MASK RCNN ORIGIN Demo](assets/step2_mask_rcnn_origin.jpg)
![MASK RCNN Label Demo](assets/step2_mask_rcnn_label.jpg)

## 3. Unet 資料前處理

[labelme_to_dataset.ipynb](labelme_to_dataset/labelme_to_dataset.ipynb)

接續 step2 輸出的資料，根據 bonding box 和 masked region 旋轉到直立，裁切出上下兩個 M 邊，作為 Unet 的輸入。

接續 step2 的範例說明：
1. 根據 bonding box 的長寬得知銅線區域偏向 y 方向
2. 找出 bonding box 中間區段（0.5 * box height）的 y 範圍
3. 找出在這個 y 範圍內的 masked region 邊界點，對每個 y 取 x 方向的最小和最大即可找出最左和最右兩排紅點
4. 根據這兩排紅點，做 Linear Regression，即可得到兩條紅線
5. 因為 Linear Regression 會取通過各點中間的線，但我們希望涵蓋更多的通線區域，所以對每個點計算點到直線的直線距離，修正截距以通過最外的點
6. 根據 Linear Regression 求得的 slope，旋轉致垂直方向
7. x 軸向裁切只留下銅線區域；y 軸向，取 masked region 的最外點選轉後的 y 值，再向外退縮一定比例後裁切
8. 裁出上下兩個正方形（M 邊）
9. 用 Labelme label 出 M 邊區域

![Lined Demo](assets/step3_lined.jpg) ![Rotated Demo](assets/step3_rotated.jpg) ![Cropped Demo](assets/step3_cropped.jpg)
![Image 1 Demo](assets/step3_img_1.jpg) ![Image 2 Demo](assets/step3_img_2.jpg)

## 4. Training Unet

[main.ipynb](image-segmentation-keras/main.ipynb)

訓練好後，計算出上下兩個 M 邊的 pixel 面積，取面積比例：(大 - 小) / 小。

![Image 1 Pixel Demo](assets/step4_img_1_pixel.jpg) ![Image 2 Pixel Demo](assets/step4_img_2_pixel.jpg) ![Result Demo](assets/step4_result.png)
