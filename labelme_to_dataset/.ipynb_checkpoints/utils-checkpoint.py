import os
import json
import yaml
import PIL.Image
from labelme import utils

def worker(q):
    while True:
        job = q.get()
        
        if job is None:
            break
        
        label = str(job["label"])
        json_file = job["json_file"]
        output_path = job["output_path"]

        out_dir = os.path.basename(json_file).replace('.json', '')
        out_dir = os.path.join(output_path, "train", label, os.path.basename(os.path.dirname(json_file)), out_dir)

        if not os.path.exists(out_dir):
            os.mkdir(out_dir)

        data = json.load(open(json_file))

        if data['imageData']:
            imageData = data['imageData']
        else:
            imagePath = os.path.join(os.path.dirname(json_file), data['imagePath'])
            with open(imagePath, 'rb') as f:
                imageData = f.read()
                imageData = base64.b64encode(imageData).decode('utf-8')
        img = utils.img_b64_to_arr(imageData)

        label_name_to_value = {'_background_': 0}
        for shape in data['shapes']:
            label_name = shape['label']
            if label_name in label_name_to_value:
                label_value = label_name_to_value[label_name]
            else:
                label_value = len(label_name_to_value)
                label_name_to_value[label_name] = label_value

        label_values, label_names = [], []
        for ln, lv in sorted(label_name_to_value.items(), key=lambda x: x[1]):
            label_values.append(lv)
            label_names.append(ln)
        assert label_values == list(range(len(label_values)))

        lbl = utils.shapes_to_label(img.shape, data['shapes'], label_name_to_value)

        captions = ['{}: {}'.format(lv, ln)
                    for ln, lv in label_name_to_value.items()]
        lbl_viz = utils.draw_label(lbl, img, captions)

        PIL.Image.fromarray(img).save(os.path.join(out_dir, 'img.png'))
        utils.lblsave(os.path.join(out_dir, 'label.png'), lbl)
        PIL.Image.fromarray(lbl_viz).save(os.path.join(out_dir, 'label_viz.png'))

        with open(os.path.join(out_dir, 'label_names.txt'), 'w') as f:
            for lbl_name in label_names:
                f.write(lbl_name + '\n')

        # warnings.warn('info.yaml is being replaced by label_names.txt')
        info = dict(label_names=label_names)
        with open(os.path.join(out_dir, 'info.yaml'), 'w') as f:
            yaml.safe_dump(info, f, default_flow_style=False)

        # print('Saved to: %s' % out_dir)